# Fog-SPKG
es: Fog-SPKG es un repositorio de paquetes .spkg para spkgmanager.
en: Fog-SPKG is a repository of .spkg packages for spkgmanager.
## Descripción
Este repositorio almacena paquetes .spkg destinados a ser utilizados con el sistema de gestión de paquetes SpkgManager. Los paquetes han sido compilados y empaquetados para facilitar su instalación y uso. Este repositorio amplía las opciones disponibles para los usuarios de SpkgManager.

## Description
This repository stores .spkg packages intended for use with the SpkgManager package management system. The packages have been compiled and packaged to facilitate easy installation and usage. This repository expands the available options for SpkgManager users.

## Acceder a las Herramientas de Spkg
Para acceder a las herramientas de Spkg, ejecute el siguiente comando en la terminal:
'create-spkg' <<<< este comando nos permitirá acceder a las herramientas de spkg tanto al gestor de paquetes 'spkgmanager' como a las opciones de empaquetamiento 'create_spkg'. 
Luego, presione `tab` para mostrar las opciones disponibles. 

##  Accessing Spkg Tools
To access the Spkg tools, run the following command in the terminal:
'create-spkg' <<<< This command allows us to access both the spkg package manager and packaging options 'create_spkg'.
Then, press tab to show available options.

## How to Use This Repository
1. **Add the Repository to SpkgManager:**
Use the add_repo option and enter the repository URL: https://gitlab.com/planet3700304/fog-spkg/-/raw/main/fog-spkg-packages.list
After adding the above line, we must update the list, so we use the 'update_list' command to get the updated list from the new repository we added.
2. **Explore and Download Packages:**
Once the repository is added, explore the list of available packages by running:
'show_available' <<< This command shows us the available packages for installation.
3. **Download specific packages with the command:**
'install_from_repo' <<<< This command allows us to install the desired package as long as it is in the repositories we have added. Remember to enter the complete package name exactly as it appears in the list using the 'show_available' command.

## Cómo Usar Este Repositorio
1. **Agregar el Repositorio a SpkgManager:**
Utilice la opción `add_repo` e ingrese la URL del repositorio: https://gitlab.com/planet3700304/fog-spkg/-/raw/main/fog-spkg-packages.list
luego de agregar la línea anterior, debemos actualizar la lista por lo que usamos el comando 'update_list' y así obtendremos la lista actualizada del nuevo repositorio que hemos agregado.

2. **Explorar y Descargar Paquetes:**
Una vez agregado el repositorio, explore la lista de paquetes disponibles ejecutando:
'show_available' <<< este comando nos mostrará los paquetes disponibles para instalar.

3. **Descargue paquetes específicos con el comando:**
'install_from_repo' <<<< este comando nos va a permitir instalar el paquete que deseamos siempre y cuando este se encuentre en los repositorios que tenemos agregados. *Recordar que debes poner el nombre completo del paquete tal cual como aparece en la lista usando el comando 'show_available'.*

## Contribuciones
Si desea contribuir o informar problemas con algún paquete, no dude en abrir un problema o enviar una solicitud de extracción.

## Contributions
If you wish to contribute or report issues with any package, feel free to open an issue or submit a pull request.

## Lista de Paquetes Disponibles - List of Available Packages
- Gparted-1.5.0-2-x86_64.spkg
- Leafpad-0.8.17-1-x86_64.spkg
- Nano-7.2-1-x86_64.spkg
- Ranger-1.9.3-3-x86_64.spkg
- Vim-9.1-1-x86_64.spkg
- Xarchiver-0.5.4.22-1-x86_64.spkg
- htop-3.3.0-1-x86_64.spkg
- Screengrab-2.7.0-2-x86_64.spkg
- Feh-3.10.2-1-x86_64.spkg
- cmus-2.10.0-2-x86_64.spkg
- neofetch-7.1.0-1-x86_64.spkg
- Links-2.29-1-x86_64.spkg
- Openbox-3.6.1-1-x86_64.spkg 
- Fluxbox-1.3.7-1-x86_64.spkg
- jwm-2.4.3-1-x86_64.spkg
- Icewm-3.4.5-1-x86_64.spkg

## Important Notes for Users

🚨 **Caution: Unstable Packages**

This repository may occasionally include packages that are marked as unstable or might have issues. Users are advised to exercise caution when installing new packages and understand that these packages are provided on an "as-is" basis.

🛑 **Reporting Issues**

If you encounter any problems, errors, or unexpected behavior with the packages, please report them immediately. Your feedback is invaluable for improving the stability and functionality of the packages. You can report issues by [creating a new issue](link to your issue tracker) in this repository.

Thank you for your cooperation and understanding.

Happy coding!

Best regards,
[@EythanEC]

## Autores y reconocimientos
Agradecemos a las siguientes personas por sus valiosas contribuciones a este proyecto:

- @EythanEC (https://gitlab.com/EythanEC) - Empaquetador principal del repositorio Fog-SPKG
- PLanet64 (https://gitlab.com/planet3700304) - Grupo donde se almacena y gestiona el repositorio
- @Tatakae57 (https://gitlab.com/Tatakae57) - Diseñador y desarrollador de spkgmanager y el kit de desarrollo spkg
- @Tatakae57 (https://gitlab.com/Tatakae57) - Tester y reportero de problemas 

## Authors and awards
We thank the following individuals for their valuable contributions to this project:
- @EythanEC (https://gitlab.com/EythanEC) - Main packager of the Fog-SPKG repository
- PLanet64 (https://gitlab.com/planet3700304) - Group where the repository is stored and managed
- @Tatakae57 (https://gitlab.com/Tatakae57) - Designer and developer of spkgmanager and the spkg development kit
- @Tatakae57 (https://gitlab.com/Tatakae57) - Tester and issue reporter

## License - Licencia
This project is under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for more details.
Este proyecto está bajo la Licencia Pública General de GNU v3.0 - ver el archivo [LICENSE](LICENSE) para más detalles.

## Project status

🚀 **Active Development**

Dear users and contributors,

This project is currently in active development, and I'm excited to continue expanding the package offerings. However, as a high school student, my availability is limited during weekdays. Expect updates and new packages primarily during the weekends (Saturday and Sunday).

Your understanding and patience are greatly appreciated. If you have any suggestions, encounter issues, or want to contribute, feel free to reach out.

Happy coding!

Best regards,
[@EythanEC]
